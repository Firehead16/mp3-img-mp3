from pydub import AudioSegment
from PIL import Image
import scipy.io.wavfile
from scipy.misc import imsave
import numpy as np
 
songData = AudioSegment.from_mp3("TestSong.mp3")
songData.export("TestSong.wav", format="wav")
print(scipy.io.wavfile.read("TestSong.wav")[1])
audioData = np.asarray(scipy.io.wavfile.read("TestSong.wav")[1], dtype=np.uint8)
pictureReshaped = np.reshape(audioData, (640, 640))
pictureOut = Image.fromarray(pictureReshaped)
pictureOut.save("PictureTest.png", "PNG")
pictojpg = Image.open("PictureTest.png")
pictojpg.save("PicJPG.jpg",quality=75) #Крутить тут
im = Image.open("PicJPG.jpg")
jpgdata = np.asarray(im.getdata(), dtype=np.int16) 
for x in range(len(jpgdata)):
    if jpgdata[x] > 200:
        jpgdata[x] = jpgdata[x]-256
scipy.io.wavfile.write('Testfrommp3.wav', 44100, jpgdata)
song = AudioSegment.from_wav("Testfrommp3.wav")
song.export("Testfrommp3.mp3", format="mp3", bitrate="320k")